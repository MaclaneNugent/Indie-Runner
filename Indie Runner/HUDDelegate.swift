//
//  HUDDelegate.swift
//  Indie Runner
//
//  Created by Maclane Nugent on 12/20/2017.
//  Copyright © 2017 Scenecio. All rights reserved.
//


import Foundation

protocol HUDDelegate {
    
    func updateCoinLabel(coins: Int)
    func addSuperCoin(index: Int)
    
}

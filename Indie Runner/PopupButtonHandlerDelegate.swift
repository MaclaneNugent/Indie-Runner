//
//  PopupButtonHandlerDelegate.swift
//  Indie Runner
//
//  Created by Maclane Nugent on 12/20/2017.
//  Copyright © 2017 Scenecio. All rights reserved.
//


import Foundation

protocol PopupButtonHandlerDelegate {
    
    func popupButtonHandler(index: Int)
    
}
